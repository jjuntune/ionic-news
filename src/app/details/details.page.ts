import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  heading: any;

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.heading = this.newsService.currentNews.author  + ' on ' + this.newsService.currentNews.publishedAt;
  }
}
