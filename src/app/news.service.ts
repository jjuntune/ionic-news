import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  currentNews: any;

  constructor(private http: HttpClient) { }

  getData(url: string): Observable<any> {
    const address = `${environment.apiUrl}/${url}&apikey=${environment.apiKey}`;
    return this.http.get(address);
  }
}
